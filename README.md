## angular2-quickstart
> This project is generated with [yo angular2-typescript generator](https://github.com/shibbir/generator-angular2-typescript) version 0.4.0.

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](http://opensource.org/licenses/MIT)

## Installation

```bash
$ npm install typescript -g
$ npm start
```

## Production Build
```bash
$ npm run build
```

## License
<a href="https://opensource.org/licenses/MIT">MIT License</a>
------------------------------------------------------------------------
项目的骨架是用yeoman框架搭建的，目前是用2.0.0 release版本
采用webpack + gulp + typescript进行编写
IDE用vscode

本项目是作为学习angularjs2的一个Demo， Demo中的实例来源于angular.cn站点的英雄实例，通过对实例的研究和站点对angularjs2的介绍，一步一步的完善英雄实例的功能

在后面的学习中，我会对代码进行一些相关的注释，以达到对angularjs2更好的理解

后期我会对代码加入单元测试，到时候会采用karma进行单元测试
