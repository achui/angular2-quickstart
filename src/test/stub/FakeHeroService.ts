import { Hero }  from  '../../app/hero';
import { HeroService } from '../../app/hero.service';
import { HEROES } from '../../app/mock-heroes'

export class FakeHeroService  {
    getHeros(): Promise<Hero[]> {
        return Promise.resolve(HEROES);
    }
}
