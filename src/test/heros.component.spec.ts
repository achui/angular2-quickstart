import {
    async, inject, ComponentFixture, TestBed,fakeAsync, tick
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Hero } from '../app/hero';
import { HeroService } from '../app/hero.service';
import { HEROES } from '../app/mock-heroes';
import { HerosComponent } from '../app/heros.component';
import { Router, RouterStub } from './stub/RouterStub';
import { FakeHeroService } from './stub/FakeHeroService';
import { newEvent } from './stub'

//////// Tests ////////
describe('HeroListComponent', () => {
    let comp: HerosComponent;
    let fixture: ComponentFixture<HerosComponent>;
    let de: DebugElement;
    let el: HTMLElement;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HerosComponent],
            providers: [
                { provide: HeroService, useClass: FakeHeroService },
                { provide: Router, useClass: RouterStub }
            ]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(HerosComponent);
            comp = fixture.componentInstance;

            fixture.detectChanges();

            return fixture.whenStable().then(() => {
                fixture.detectChanges();
            })
        })

    }));

    it('list heros after ngOnInit', () => {
        const expectedHero = HEROES[0];
        let heroRows: HTMLLIElement[] = fixture.debugElement.queryAll(By.css('li')).map(de => de.nativeElement);
        expect(heroRows.length).toBe(10, 'list hero');
        expect(heroRows[0].textContent).toContain(expectedHero.name);

    });

    it('should select heor on click', () => {
        const expectedHero = HEROES[1];
        let heroRows: HTMLLIElement[] = fixture.debugElement.queryAll(By.css('li')).map(de => de.nativeElement);
        const li = heroRows[1];
        li.dispatchEvent(newEvent('click'))
        //tick();
        expect(comp.selectedHero).toEqual(expectedHero);
    });

    it('shoulde navgiate to seleted hero detail on click', fakeAsync(() => {
        const expectedHero = HEROES[1];
        let heroRows: HTMLLIElement[] = fixture.debugElement.queryAll(By.css('li')).map(de => de.nativeElement);
        const button = heroRows[1].children[3];
        expect(button.textContent).toContain('update', 'find update button');
        expect(heroRows[1].children.length).toBe(4, 'find update button');
        const router = fixture.debugElement.injector.get(Router);
        let navSpy = spyOn(router, 'navigate');
        button.dispatchEvent(newEvent('click'));
        tick();

         // should have navigated
        expect(navSpy.calls.any()).toBe(true, 'navigate called');

        const navArgs = navSpy.calls.first().args[0];
        console.dir(navArgs);
        expect(navArgs[0]).toContain('detail', 'nav to heroes detail URL');
        expect(navArgs[1]).toBe(expectedHero.id, 'expeted hero.id');
    }));
})
