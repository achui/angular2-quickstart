import { async, inject, ComponentFixture, TestBed
} from '@angular/core/testing';

import { MockBackend, MockConnection } from '@angular/http/testing';

import {
  HttpModule, Http, XHRBackend, Response, ResponseOptions
} from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { Hero } from '../app/hero';
import { HeroService } from '../app/hero.service';
import {Logger} from '../app/logger.service'
import { HEROES } from '../app/mock-heroes';

//////// Tests ////////
describe('Http-HeroService (mockBackend)', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                HeroService,
                {provide: XHRBackend, useClass: MockBackend},
                Logger
            ]
        })
    }));

    it('can instantiate service when inject service', inject([Http,Logger], (http: Http, logger:Logger) => {
        expect(http).not.toBeNull('http should be provided');
        let service = new HeroService(http, logger, true);
        expect(service instanceof HeroService).toBe(true, 'new service should be ok');
    }));

    it('can provide the mockBackend as XHRBackend', inject([XHRBackend],(backend: MockBackend) => {
        console.dir(backend);
        expect(backend).not.toBeNull('backend should be provideed');
    }));

    describe('when getHeroes', () => {
        let backend: MockBackend;
        let service: HeroService;
        let response: Response;

        beforeEach(inject([Http, XHRBackend, Logger], (http: Http, be: MockBackend, logger: Logger) => {
            backend = be;
            service = new HeroService(http, logger, true);
            let options = new ResponseOptions({
                status: 200,
                body: {
                    data: HEROES
                }
            });
            response = new Response(options);
        }));

        it('should have expected fake heros (then)', async(inject([], ()=> {
            backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));
            service.getHeros().then(heroes => {
                expect(heroes.length).toBe(HEROES.length,'should be have excpetd no. of heores');
            });
        })));

        it('shoulde be ok returning no heroes', async(inject([], () => {
            let resp = new Response(new ResponseOptions({
                status: 200,
                body:{
                    data: []
                }
            }));
            backend.connections.subscribe((c: MockConnection) => {
                c.mockRespond(resp);
            });
            service.getHeros().then( heroes => {
                expect(heroes.length).toBe(0, 'shoulde have no heroes');
            });
        })));

        it('should treat 404 as an error', async(inject([], () => {
            let resp = new Response(new ResponseOptions({
                status: 404
            }));
            backend.connections.subscribe((c: MockConnection) => {
                c.mockRespond(resp);
            });
            service.getHeros().catch(reason => {
                expect(reason).toMatch(/Bad response status/,  'should catch bad response status code');
            })
        })));
    })
})
