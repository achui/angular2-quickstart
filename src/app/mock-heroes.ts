import { Hero } from './hero';
export const HEROES: Hero[] = [
    { id: 11, name: 'Mr. Nice', power: '', alterEgo: '', isSecret:false },
    { id: 12, name: 'Narco', power: '', alterEgo: '', isSecret:false },
    { id: 13, name: 'Bombasto', power: '', alterEgo: '', isSecret:false },
    { id: 14, name: 'Celeritas', power: '', alterEgo: '', isSecret:false },
    { id: 15, name: 'Magneta', power: '', alterEgo: '', isSecret:false },
    { id: 16, name: 'RubberMan', power: '', alterEgo: '', isSecret:false },
    { id: 17, name: 'Dynama', power: '', alterEgo: '', isSecret:false },
    { id: 18, name: 'Dr IQ', power: '', alterEgo: '', isSecret:false },
    { id: 19, name: 'Magma', power: '', alterEgo: '', isSecret:false },
    { id: 20, name: 'Tornado', power: '', alterEgo: '', isSecret:false }
];
