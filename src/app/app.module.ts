import {NgModule} from '@angular/core';
import { BrowserModule} from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {HerosComponent} from './heros.component'
import { HeroDetailComponent } from './hero-detail.component'
import {DashboardComponent} from './dashboard.component'
import {HeroSearchComponent} from './hero-search.component'
import { HeroFormComponent } from './hero-form.component';
import {HeroService} from './hero.service'
import {UserService} from './user.service'
import {Logger} from './logger.service'
import {routing} from './app.routes'

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular2-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

@NgModule({
    imports:[
        BrowserModule,
        FormsModule,
        HttpModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService),
        routing
    ],
    declarations:[
        AppComponent,
        HeroDetailComponent,
        HerosComponent,
        HeroFormComponent,
        DashboardComponent,
        HeroSearchComponent
    ],
    providers:[Logger,UserService],
    bootstrap:[
        AppComponent
    ]
})

export class AppModule {}