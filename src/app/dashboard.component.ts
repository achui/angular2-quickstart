import { Component, OnInit } from '@angular/core';
import {Hero} from  './hero'
import {HeroService} from './hero.service'
import { Router } from '@angular/router';
import {Logger} from './logger.service';
import { heroServiceProvider } from './hero.service.provider';
@Component({
    selector: 'my-dashboard',
    template: require('./dashboard.html'),
    styles: [require('./dashboard.css')],
    providers: [heroServiceProvider]
})
export class DashboardComponent implements OnInit {
    heroes: Hero[] = [];
    constructor(private heroService: HeroService,
        private router: Router) { }

    ngOnInit() {
        this.heroService.getHeros().then(heroes => this.heroes = heroes.slice(1, 5));
    }

    gotoDetail(hero: Hero): void {
        let link = ['/detail', hero.id];
        this.router.navigate(link);
    }

}