import {Component, OnInit} from '@angular/core';
import {Logger} from './logger.service';
import {HeroService} from './hero.service';
import {UserService} from './user.service'
import { heroServiceProvider } from './hero.service.provider';

//import {ROUTER_DIRECTIVES} from '@angular/router';

// import {HomeComponent} from './home/home.component';
// import {AboutComponent} from './about/about.component';

import 'bootstrap/dist/css/bootstrap.css';
import '../styles.css';
@Component({
    selector: 'my-app',
    template: `
    <h1>{{title}}</h1>
    <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
    <a routerLink="/heros" routerLinkActive="active">Heros</a>
    <a routerLink="/hero" routerLinkActive="active">Add</a>
    <router-outlet></router-outlet>
    `,
    styles: [require('./app.css')],
    //providers: [HeroService, Logger, UserService]
    //全局通用的service的注入，可以放这边，叫做根基本的注入
    providers: [UserService,heroServiceProvider]
})

export class AppComponent {
    title = 'Tour of Heroes';

}

