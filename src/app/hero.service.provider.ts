/**
 * 这个类演示了，如何通过工厂提供商，进行注入，
 * HeroService的注入，是通过工厂方法 heroServiceFactory来生成实例的
 * 用这种方法可以实现一个Service，跟据不同的实例，获取不同的对象，来实现业务逻辑，有点像工厂模式
 */
import {Logger} from './logger.service';
import {UserService} from './user.service'
import {HeroService} from './hero.service'
import {Http} from '@angular/http'
let heroServiceFactory = (http: Http, logger: Logger, userService: UserService) => {
    return new HeroService(http, logger, userService.user.isAuthorized);
}

export let heroServiceProvider = {
    provide: HeroService,
    useFactory: heroServiceFactory,
    deps: [Http, Logger, UserService] //构造函数需要多少个参数，这边的deps也需要多少个
}
