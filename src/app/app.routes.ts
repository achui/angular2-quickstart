import {Routes, RouterModule} from '@angular/router'
import {ModuleWithProviders} from '@angular/core'
import {HerosComponent} from './heros.component'
import {DashboardComponent} from './dashboard.component'
import {HeroDetailComponent} from './hero-detail.component'
import {HeroFormComponent} from './hero-form.component'

const appRoutes: Routes = [{
  path: 'heros',
  component: HerosComponent
}, {
    path: 'dashboard',
    component: DashboardComponent
  },{
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },{
    path:'detail/:id',
    component: HeroDetailComponent
  },{
    path:'hero',
    component: HeroFormComponent
  }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);