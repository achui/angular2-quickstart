import { Component, OnInit } from '@angular/core';
import {Hero} from './hero'

@Component({
    selector: 'hero-form',
    template: require('./hero-form.html')
})
export class HeroFormComponent implements OnInit {
    powers = ['Really Smart', 'Super Flexible',
        'Super Hot', 'Weather Changer'];

    model = new Hero(18, 'Dr IQ', this.powers[0], 'Chuck Overstreet');

    submitted = false;

    active = true;

    constructor() { }

    ngOnInit() { }

    onSubmit(){
        this.submitted = true;
    }

    get diagnostic(){
        return JSON.stringify(this.model);
    }

    newHero(): void {
        this.model = new Hero(42, '','');
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }
}