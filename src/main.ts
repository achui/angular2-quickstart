import {platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';

import {AppModule} from './app/app.module';
//import {APP_ROUTER_PROVIDERS } from './app/app.routes';

if (process.env.NODE_ENV === 'production') {
    enableProdMode();
}

platformBrowserDynamic ().bootstrapModule(AppModule)

